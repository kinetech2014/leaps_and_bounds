class CreateSignInOuts < ActiveRecord::Migration
  def change
    create_table :sign_in_outs do |t|
      t.datetime :date
      t.datetime :sign_in
      t.datetime :sign_out
      t.string :status
      t.integer :student_id

      t.timestamps
    end
  end
end
