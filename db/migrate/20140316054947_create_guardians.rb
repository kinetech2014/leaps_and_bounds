class CreateGuardians < ActiveRecord::Migration
  def change
    create_table :guardians do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :relationship
      t.string :authorized_pickup
      t.string :sex
      t.string :driver_license_number
      t.string :email
      t.string :primary_phone
      t.string :secondary_phone
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip_code

      t.timestamps
    end
  end
end
