class ChangeTypeColumnNameInMedicalIssues < ActiveRecord::Migration
  def change
    rename_column :medical_issues, :type, :issue_type
  end
end
