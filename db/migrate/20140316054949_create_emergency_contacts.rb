class CreateEmergencyContacts < ActiveRecord::Migration
  def change
    create_table :emergency_contacts do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :relationship
      t.string :primary_phone
      t.string :secondary_phone

      t.timestamps
    end
  end
end
