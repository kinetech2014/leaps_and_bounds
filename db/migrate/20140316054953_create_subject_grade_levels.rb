class CreateSubjectGradeLevels < ActiveRecord::Migration
  def change
    create_table :subject_grade_levels do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
