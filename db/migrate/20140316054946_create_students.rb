class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.date :birth_date
      t.string :sex
      t.decimal :tutoring_hours
      t.string :primary_phone
      t.string :secondary_phone
      t.string :email
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :school_name

      t.timestamps
    end
  end
end
