class RemoveNameFromMedicalIssue < ActiveRecord::Migration
  def change
    remove_column :medical_issues, :name
  end
end
