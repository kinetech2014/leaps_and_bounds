class CreateStudentTeachers < ActiveRecord::Migration
  def change
    create_table :student_teachers do |t|
      t.references :student, index: true
      t.references :teacher, index: true

      t.timestamps
    end
  end
end
