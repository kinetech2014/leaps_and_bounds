class CreateStudentSubjectGradeLevels < ActiveRecord::Migration
  def change
    create_table :student_subject_grade_levels do |t|
      t.integer :subject_grade_level_id
      t.integer :student_id

      t.timestamps
    end
  end
end
