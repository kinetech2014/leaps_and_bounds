class ChangeStudentIdInEmergencyContactToInteger < ActiveRecord::Migration
  def change
    change_column :emergency_contacts, :student_id, :integer

  end
end
