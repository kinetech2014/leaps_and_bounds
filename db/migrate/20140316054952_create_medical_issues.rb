class CreateMedicalIssues < ActiveRecord::Migration
  def change
    create_table :medical_issues do |t|
      t.string :name
      t.string :type
      t.string :description
      t.string :notes
      t.integer :student_id

      t.timestamps
    end
  end
end
