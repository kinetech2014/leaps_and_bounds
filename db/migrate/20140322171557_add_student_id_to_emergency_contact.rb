class AddStudentIdToEmergencyContact < ActiveRecord::Migration
  def change
    add_column :emergency_contacts, :student_id, :string
  end
end
