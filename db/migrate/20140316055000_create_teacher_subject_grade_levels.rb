class CreateTeacherSubjectGradeLevels < ActiveRecord::Migration
  def change
    create_table :teacher_subject_grade_levels do |t|
      t.integer :teacher_id
      t.integer :subject_grade_level_id

      t.timestamps
    end
  end
end
