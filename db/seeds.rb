# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

EmergencyContact.delete_all
Guardian.delete_all
MedicalIssue.delete_all
SignInOut.delete_all
StudentGuardian.delete_all
StudentTeacher.delete_all
Student.delete_all
SubjectGradeLevel.delete_all
Teacher.delete_all

Student.create([
    {id: 100001, first_name: 'Timmy', middle_name: 'Ted',last_name: 'Smith', birth_date: '1999-03-16', sex: 'Male', tutoring_hours: 10, primary_phone: '713-254-9867', secondary_phone: '', email: 't.smith@gmail.com', address1: '123 Fake St', address2: 'Apt 123', city: 'Spring', state: 'TX', zip_code: '77463', school_name: 'Spring Elementary'},
    {id: 100002, first_name: 'Sarah', middle_name: '', last_name: 'Smith', birth_date: '1997-03-13', sex: 'Female', tutoring_hours: 6, primary_phone: '281-487-2398', secondary_phone: '', email: 's.smith@comcast.net', address1: '123 Fake St', address2: 'Apt 123', city: 'Spring', state: 'TX', zip_code: '77463', school_name: 'George Anderson Elementary'},
    {id: 100003, first_name: 'Joshua',middle_name: '', last_name: 'Garcia', birth_date: '2000-03-16', sex: 'Male', tutoring_hours: 2, primary_phone: '789-456-1230', secondary_phone: '', email: 'j.garcia@yahoo.com', address1: '123 Fake St', address2: 'Apt 123', city: 'Spring', state: 'TX', zip_code: '77463', school_name: 'Bammel Elementary'},
    {id: 100004, first_name: 'Angela',middle_name: '', last_name: 'Rojas', birth_date: '2001-04-13', sex: 'Female', tutoring_hours: 5, primary_phone:'234-765-3678', secondary_phone: '', email: 'a.rojas@sbcglobal.net', address1:'234 Rose St', address2: 'Apt 145', city: 'Houston', state: 'TX', zip_code: '77045', school_name: 'Joseph Elementary'},
    {id: 100005, first_name: 'Robert',middle_name: '', last_name: 'Williams', birth_date: '2002-05-18', sex: 'Male', tutoring_hours: 6, primary_phone:'832-409-5723', secondary_phone: '', email: 'rob.will31@gmail.com', address1:'1256 Smith St', address2: 'Apt 132', city: 'Spring', state: 'TX', zip_code: '77047', school_name: 'Carolee Booker Elementary'},
    {id: 100006, first_name: 'Kaleb', middle_name: '', last_name: 'Ojeda', birth_date: '2002-04-13', sex: 'Male', tutoring_hours: 7, primary_phone:'832-487-1029', secondary_phone: '', email: 'ojeda42.kal@yahoo.com', address1:'1432 Lamar St', address2: 'Apt 345', city: 'Houston', state: 'TX', zip_code: '77045', school_name: 'Chet Burchett Elementary'},
    {id: 100007, first_name: 'Tony',  middle_name: '', last_name: 'Matthews', birth_date: '2000-05-20', sex: 'Male', tutoring_hours: 4, primary_phone:'281-330-8004', secondary_phone: '', email: 'matt1989@gmail.com', address1:'234 Rose St', address2: 'Apt 256', city: 'Spring', state: 'TX', zip_code: '77048', school_name: 'Milton Cooper Elementary'},
    {id: 100008, first_name: 'Maria', middle_name: '', last_name: 'Rodriguez', birth_date: '2002-06-20', sex: 'Female', tutoring_hours: 6, primary_phone:'281-414-9222', secondary_phone: '', email: 'skynet42maria@windstream.net', address1:'234 Lamar St', address2: 'Apt 234', city: 'Houston', state: 'TX', zip_code: '77042', school_name: 'Ralph Elementary'},
    {id: 100009, first_name: 'Thomas',middle_name: '', last_name: 'Smith', birth_date: '1999-07-04', sex: 'Male', tutoring_hours: 3, primary_phone:'281-300-3450', secondary_phone: '', email: 'tommy.s007@hotmail.com', address1:'2344 Flower St', address2: 'Apt 1234', city: 'Spring', state: 'TX', zip_code: '77046', school_name: 'Heritage Elementary'},
    {id: 100010, first_name: 'Rick',middle_name: '', last_name: 'Blanc', birth_date: '1998-10-11', sex: 'Male', tutoring_hours: 4, primary_phone:'713-334-9946', secondary_phone: '', email: 'caravela17@aol.com', address1:'234 Rose St', address2: 'Apt 145', city: 'Houston', state: 'TX', zip_code: '77045', school_name: 'Pearl Elementary'},
    {id: 100011, first_name: 'Aladdin',middle_name: '', last_name: 'Wilder', birth_date: '2001-05-20', sex: 'Male', tutoring_hours: 5, primary_phone:'281-113-0498', secondary_phone: '', email: 'wilderpom@gmail.com', address1:'7794 Tempus Street', address2: '', city: 'Houston', state: 'TX', zip_code: '77045', school_name: 'Donna Lewis Elementary'},
    {id: 100012, first_name: 'Tarik',middle_name: '', last_name: 'Mcgowan', birth_date: '1999-03-19', sex: 'Male', tutoring_hours: 6, primary_phone:'281-946-6464', secondary_phone: '', email: 'mcgowan.industries@comcast.net', address1:'P.O. Box 904, 303 Commodo St.', address2: 'Apt 234', city: 'Spring', state: 'TX', zip_code: '77034', school_name: 'Helen Major Elementary'},
    {id: 100013, first_name: 'Sharon',middle_name: '', last_name: 'Ball', birth_date: '2000-12-13', sex: 'Female', tutoring_hours: 8, primary_phone:'713-103-4932', secondary_phone: '', email: 'fallball68@windstream.net', address1:'4132 Neque St', address2: 'Apt 145', city: 'Houston', state: 'TX', zip_code: '77045', school_name: 'Northgate Crossing Elementary'},
    {id: 100014, first_name: 'Brian',middle_name: '', last_name: 'Cole', birth_date: '1998-01-17', sex: 'Male', tutoring_hours: 4, primary_phone:'281-449-3336', secondary_phone: '', email: 'b.cole461@yahoo.com', address1:'234 Rose St', address2: 'P.O. Box 796, 4404 Ac St.', city: 'Spring', state: 'TX', zip_code: '77465', school_name: 'Salyers Elementary'},
    {id: 100015, first_name: 'Patrick',middle_name: '', last_name: 'Hill', birth_date: '2001-03-27', sex: 'Male', tutoring_hours: 8, primary_phone:'832-867-8282', secondary_phone: '', email: 'hilly@gmail.com', address1:'440-4413 Justo St', address2: 'Apt 145', city: 'Houston', state: 'TX', zip_code: '77452', school_name: 'Deloras Thomson Elementary'},
    {id: 100016, first_name: 'Veronica',middle_name: '', last_name: 'Vance', birth_date: '2000-06-19', sex: 'Female', tutoring_hours: 6, primary_phone:'281-380-8192', secondary_phone: '', email: 'veronic4@aol.com', address1:'P.O. Box 960, 9145 Arcu Avenue', address2: 'Apt 145', city: 'Spring', state: 'TX', zip_code: '77045', school_name: 'Pat Reynolds Elementary'},
    {id: 100017, first_name: 'Jonah',middle_name: '', last_name: 'Horn', birth_date: '2001-11-10', sex: 'Male', tutoring_hours: 9, primary_phone:'281-530-9060', secondary_phone: '', email: 'jonahhill5@comcast.net', address1:'2673 Mauris St', address2: 'Apt 1906', city: 'Houston', state: 'TX', zip_code: '77234', school_name: 'Ponderosa Elementary'},
    {id: 100018, first_name: 'Ruben',middle_name: '', last_name: 'Ortiz', birth_date: '1997-03-23', sex: 'Male', tutoring_hours: 8, primary_phone:'713-666-4403', secondary_phone: '', email: 'ortiz.rob@gmail.com', address1:'234 Rose St', address2: 'Apt 145', city: 'Houston', state: 'TX', zip_code: '77245', school_name: 'Lewis Eugene Elementary'},
    {id: 100019, first_name: 'Ciara',middle_name: '', last_name: 'Garrett', birth_date: '1998-03-25', sex: 'Female', tutoring_hours: 4, primary_phone:'281-713-8324', secondary_phone: '', email: '24gtrgarret@haltech.com', address1:'P.O. Box 214, 4755 Donec Road', address2: 'Apt 456', city: 'Houston', state: 'TX', zip_code: '77034', school_name: 'Smith Elementary'},
    {id: 100020, first_name: 'Rooney',middle_name: '', last_name: 'May', birth_date: '2001-04-13', sex: 'Female', tutoring_hours: 7, primary_phone:'712-281-8321', secondary_phone: '', email: 'may.r@aol.com', address1:'206-7308 Suspendisse Av.', address2: 'Apt 234', city: 'Houston', state: 'TX', zip_code: '77145', school_name: 'Rickey Bailey Elementary'},
    {id: 100021, first_name: 'Lila',middle_name: '', last_name: 'Noble', birth_date: '1999-03-20', sex: 'Female', tutoring_hours: 4, primary_phone:'281-321-9902', secondary_phone: '', email: 'f.n.stump2@gmail.com', address1:'8339 Est, Avenue', address2: 'Apt 567', city: 'Spring', state: 'TX', zip_code: '77545', school_name: 'Stelle Clughton Elementary'},
    {id: 100022, first_name: 'Carl',middle_name: '', last_name: 'Bond', birth_date: '1996-09-10', sex: 'Male', tutoring_hours: 3, primary_phone:'832-110-9912', secondary_phone: '', email: '007bond@hotmail.com', address1:'P.O. Box 150, 6663 Eu, Street', address2: '', city: 'Houston', state: 'TX', zip_code: '77245', school_name: 'Edwin Wells Elementary'},
    {id: 100023, first_name: 'Troy',middle_name: '', last_name: 'Quinn', birth_date: '1999-01-06', sex: 'Male', tutoring_hours: 2, primary_phone:'281-550-9713', secondary_phone: '', email: 'quinny43@hotmail.com', address1:'P.O. Box 137, 1555 Hendrerit Avenue', address2: '', city: 'Houston', state: 'TX', zip_code: '77345', school_name: 'Westfield Elementary'},
    {id: 100024, first_name: 'Sarah',middle_name: '', last_name: 'Salinas', birth_date: '2000-05-07', sex: 'Female', tutoring_hours: 5, primary_phone:'832-771-2209', secondary_phone: '', email: 'sarah.s@aol.com', address1:'5821 Orci Road', address2: '', city: 'Spring', state: 'TX', zip_code: '77478', school_name: 'Spring Elementary'},
    {id: 100025, first_name: 'Sarah',middle_name: '', last_name: 'Garza', birth_date: '2000-05-07', sex: 'Female', tutoring_hours: 5, primary_phone:'281-444-0012', secondary_phone: '', email: 'g4rz4@yahoo.com', address1:'5821 Orci Road', address2: '', city: 'Spring', state: 'TX', zip_code: '77478', school_name: 'Spring Elementary'}
               ])

Guardian.create([
    {first_name: 'John', middle_name: '', last_name: 'Smith', relationship: 'Father', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '82312398', email: 'dad@smith.com', primary_phone: '123-456-7890', secondary_phone: '', address1: '123 Fake St', address2: 'Apt 123', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Megan', middle_name: '', last_name: 'Smith', relationship: 'Mother', authorized_pickup: 'Yes', sex: 'Female', driver_license_number: '12312312', email: 'mom@smith.com', primary_phone: '123-456-7890', secondary_phone: '', address1: '123 Fake St', address2: 'Apt 123', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Juan', middle_name: '', last_name: 'Garcia', relationship: 'Father', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '48593842', email: 'juan@garcia.com', primary_phone: '847-384-9384', secondary_phone: '', address1: '123 Fake St', address2: 'Apt 123', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Martha', middle_name: '', last_name: 'Williams', relationship: 'Mother', authorized_pickup: 'Yes', sex: 'Female', driver_license_number: '64523785', email: 'mwilliams@gmail.com', primary_phone: '987-047-6587', secondary_phone: '', address1: '5821 Orci Road', address2: '', city: 'Spring', state: 'TX', zip_code: '77478'},
    {first_name: 'Mario', middle_name: '', last_name: 'Smith', relationship: 'Father', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '48593842', email: 'msmith@yahoo.com', primary_phone: '847-384-9384', secondary_phone: '', address1: '123 Fake St', address2: 'Apt 123', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Teresa', middle_name: '', last_name: 'Quinn', relationship: 'Mother', authorized_pickup: 'Yes', sex: 'Female', driver_license_number: '4567389', email: 'tquinn@hotmail.com.com', primary_phone: '847-384-9384', secondary_phone: '', address1: 'P.O. Box 137, 1555 Hendrerit Avenue', address2: '', city: 'Spring', state: 'TX', zip_code: '77456'},
    {first_name: 'Jay', middle_name: '', last_name: 'Nobel', relationship: 'Father', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '3269302', email: 'jnobel@gmail.com', primary_phone: '745-632-7890', secondary_phone: '', address1: 'P.O. Box 150, 6663 Eu, Street', address2: '', city: 'Houston', state: 'TX', zip_code: '77245'},
    {first_name: 'Patrick', middle_name: '', last_name: 'May', relationship: 'Father', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '3245672', email: 'pmay@yahoo.com', primary_phone: '956-473-8234', secondary_phone: '', address1: '206-7308 Suspendisse Av.', address2: 'Apt 234', city: 'Houston', state: 'TX', zip_code: '77145'},
    {first_name: 'Michael', middle_name: '', last_name: 'Garret', relationship: 'Father', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '6342612', email: 'mgarret@hotmail.com', primary_phone: '945-362-7389', secondary_phone: '', address1: 'P.O. Box 214, 4755 Donec Road', address2: 'Apt 456', city: 'Houston', state: 'TX', zip_code: '77034'},
    {first_name: 'Sharon', middle_name: '', last_name: 'Ortiz', relationship: 'Mother', authorized_pickup: 'Yes', sex: 'Female', driver_license_number: '4532789', email: 'sortiz@gmail.com', primary_phone: '234-765-3678', secondary_phone: '', address1: '234 Rose St', address2: 'Apt 145', city: 'Spring', state: 'TX', zip_code: '77465'},
    {first_name: 'Veronica', middle_name: '', last_name: 'Horn', relationship: 'Mother', authorized_pickup: 'Yes', sex: 'Female', driver_license_number: '2547801', email: 'vhorn@yahoo.com', primary_phone: '951-472-9616', secondary_phone: '', address1: '2673 Mauris St', address2: 'Apt 1906', city: 'Houston', state: 'TX', zip_code: '77463'},
    {first_name: 'Rhona', middle_name: '', last_name: 'Ray', relationship: 'Mother', authorized_pickup: 'Yes', sex: 'Female', driver_license_number: '9436528', email: 'rray@gmail.com', primary_phone: '847-384-9384', secondary_phone: '', address1: '123 Fake St', address2: '', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Susane', middle_name: '', last_name: 'Valentine', relationship: 'Mother', authorized_pickup: 'Yes', sex: 'Female', driver_license_number: '9456342', email: 'svalentine@gmail.com', primary_phone: '847-384-9384', secondary_phone: '', address1: '4132 Neque St', address2: 'Apt145', city: 'Houston', state: 'TX', zip_code: '77045'},
    {first_name: 'Audra', middle_name: '', last_name: 'Green', relationship: 'Mother', authorized_pickup: 'Yes', sex: 'Female', driver_license_number: '8453725', email: 'agreen@yahoo.com', primary_phone: '436-278-9125', secondary_phone: '', address1: 'P.O. Box 904, 303 Commodo St', address2: 'Apt 123', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Sarah', middle_name: '', last_name: 'Salinas', relationship: 'Mother', authorized_pickup: 'Yes', sex: 'Female', driver_license_number: '48593842', email: 'dad@garcia.com', primary_phone: '847-384-9384', secondary_phone: '', address1: '123 Fake St', address2: 'Apt 123', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Dante', middle_name: '', last_name: 'Owens', relationship: 'Brother', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '3729812', email: 'dowens@yahoo.com', primary_phone: '847-384-9384', secondary_phone: '', address1: '6753 Dictum Rd.', address2: '', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Issac', middle_name: '', last_name: 'Ortega', relationship: 'Father', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '7659234', email: 'iortega@yahoo.com', primary_phone: '534-267-8932', secondary_phone: '', address1: 'P.O. Box 172, 6612 Ultrices. St.', address2: '', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Claudia', middle_name: '', last_name: 'Boyle', relationship: 'Mother', authorized_pickup: 'Yes', sex: 'Female', driver_license_number: '48593842', email: 'cboyle@hotmail.com', primary_phone: '436-782-3980', secondary_phone: '', address1: 'P.O. Box 407, 2640 Justo. Ave', address2: '', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Geoffery', middle_name: '', last_name: 'Santos', relationship: 'Father', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '8674523', email: 'gsantos@yahoo.com', primary_phone: '230-237-5684', secondary_phone: '', address1: 'P.O. Box 284, 6560 Massa Avenue', address2: '', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Leonard', middle_name: '', last_name: 'Anthony', relationship: 'Father', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '3498765', email: 'lanthony@gmail.com', primary_phone: '832-345-6790', secondary_phone: '', address1: '327-7078 Vestibulum Ave', address2: '', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Finn', middle_name: '', last_name: 'Willis', relationship: 'Father', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '6578329', email: 'fwillis@yahoo.com', primary_phone: '281-367-8902', secondary_phone: '', address1: '4964 Eu Rd.', address2: '', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Gage', middle_name: '', last_name: 'Powell', relationship: 'Brother', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '2673490', email: 'gpowell@gmail.com', primary_phone: '342-567-2345', secondary_phone: '', address1: 'P.O. Box 932, 4382 Sit St.', address2: '', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Adrian', middle_name: '', last_name: 'Norton', relationship: 'Father', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '3671903', email: 'anorton@hotmail.com', primary_phone: '713-456-3245', secondary_phone: '', address1: '5958 Arcu. Rd.', address2: '', city: 'Houston', state: 'TX', zip_code: '77463'},
    {first_name: 'Holly', middle_name: '', last_name: 'Mclean', relationship: 'Mother', authorized_pickup: 'Yes', sex: 'Female', driver_license_number: '3265478', email: 'hmclean@gmail.com', primary_phone: '832-247-8901', secondary_phone: '', address1: '960-1743 Turpis. Ave', address2: '', city: 'Spring', state: 'TX', zip_code: '77463'},
    {first_name: 'Felix', middle_name: '', last_name: 'Cabrera', relationship: 'Father', authorized_pickup: 'Yes', sex: 'Male', driver_license_number: '3426573', email: 'fcabrera@hotmail.com', primary_phone: '281-567-0987', secondary_phone: '', address1: 'P.O. Box 529, 412 Et Ave', address2: '', city: 'Spring', state: 'TX', zip_code: '77463'}
            ])

StudentGuardian.create([
    {guardian_id: 1, student_id: 100001},
    {guardian_id: 2, student_id: 100002},
    {guardian_id: 3, student_id: 100003},
    {guardian_id: 4, student_id: 100004},
    {guardian_id: 5, student_id: 100005},
    {guardian_id: 6, student_id: 100006},
    {guardian_id: 7, student_id: 100007},
    {guardian_id: 8, student_id: 100008},
    {guardian_id: 9, student_id: 100009},
    {guardian_id: 10, student_id: 100010},
    {guardian_id: 11, student_id: 100011},
    {guardian_id: 12, student_id: 100012},
    {guardian_id: 13, student_id: 100013},
    {guardian_id: 14, student_id: 100014},
    {guardian_id: 15, student_id: 100015},
    {guardian_id: 16, student_id: 100016},
    {guardian_id: 17, student_id: 100017},
    {guardian_id: 18, student_id: 100018},
    {guardian_id: 19, student_id: 100019},
    {guardian_id: 20, student_id: 100020},
    {guardian_id: 21, student_id: 100021},
    {guardian_id: 22, student_id: 100022},
    {guardian_id: 23, student_id: 100023},
    {guardian_id: 24, student_id: 100024},
    {guardian_id: 25, student_id: 100025}
                       ])

Teacher.create([
    {id: 100001,first_name: 'Christian', middle_name: '', last_name: 'Goudeau', sex: 'Male'},
    {id: 100002,first_name: 'Taylor', middle_name: '', last_name: 'Blake', sex: 'Female'},
    {id: 100003,first_name: 'Gloria', middle_name: 'J', last_name: 'Greenwood', sex: 'Female'},
    {id: 100004,first_name: 'Patricia', middle_name: '', last_name: 'Pettit', sex: 'Female'},
    {id: 100005,first_name: 'Artistell', middle_name: '', last_name: 'Carter', sex: 'Female'},
    {id: 100006, first_name: 'DeShalette', middle_name: '', last_name: 'Polk', sex: 'Female'},
    {id: 100007,first_name: 'Pamela', middle_name: '', last_name: 'Josey', sex: 'Female'},
    {id: 100008,first_name: 'Kelli', middle_name: '', last_name: 'Burleigh', sex: 'Female'},
    {id: 100009,first_name: 'Eric', middle_name: '', last_name: 'Remigio', sex: 'Male'},
    {id: 100010,first_name: 'Maria', middle_name: '', last_name: 'Longoria', sex: 'Female'},
    {id: 100011,first_name: 'Ava', middle_name: '', last_name: 'Howard', sex: 'Female'}
                ])

EmergencyContact.create([
    {first_name: 'Michael', middle_name: '', last_name: 'Smith', relationship: 'Cousin', primary_phone: '8765489036', secondary_phone: '', student_id: 100001},
    {first_name: 'Robert', middle_name: '', last_name: 'Meyes', relationship: 'Friend', primary_phone: '2340985645', secondary_phone: '', student_id: 100002},
    {first_name: 'Marrissa', middle_name: '', last_name: 'Diaz', relationship: 'Grandmother', primary_phone: '7465389080', secondary_phone: '', student_id: 100003},
    {first_name: 'Karla', middle_name: '', last_name: 'Martinez', relationship: 'Sister', primary_phone: '6432789254', secondary_phone: '', student_id: 100004},
    {first_name: 'Perla', middle_name: '', last_name: 'Bell', relationship: 'Sister', primary_phone: '7340924364', secondary_phone: '', student_id: 100005},
    {first_name: 'Jacob', middle_name: '', last_name: 'Howard', relationship: 'Brother', primary_phone: '8340956734', secondary_phone: '', student_id: 100006},
    {first_name: 'Russel', middle_name: '', last_name: 'Stewart', relationship: 'Friend', primary_phone: '7456358210', secondary_phone: '', student_id: 100007},
    {first_name: 'Rachel', middle_name: '', last_name: 'Berry', relationship: 'Friend', primary_phone: '9236517845', secondary_phone: '', student_id: 100008},
    {first_name: 'Finn', middle_name: '', last_name: 'Scott', relationship: 'Friend', primary_phone: '3245879083', secondary_phone: '', student_id: 100009},
    {first_name: 'Mario', middle_name: '', last_name: 'Lopez', relationship: 'Friend', primary_phone: '2634874652', secondary_phone: '', student_id: 100010},
    {first_name: 'Ron', middle_name: '', last_name: 'Smith', relationship: 'Brother', primary_phone: '5342678453', secondary_phone: '', student_id: 100011},
    {first_name: 'Stan', middle_name: '', last_name: 'Whright', relationship: 'Cousin', primary_phone: '7634092143', secondary_phone: '', student_id: 100012}
                       ])


StudentTeacher.create ([
    {teacher_id: 100001, student_id: 100002},
    {teacher_id: 100002, student_id: 100002},
    {teacher_id: 100003, student_id: 100003},
    {teacher_id: 100004, student_id: 100004},
    {teacher_id: 100005, student_id: 100005},
    {teacher_id: 100006, student_id: 100006},
    {teacher_id: 100007, student_id: 100007},
    {teacher_id: 100008, student_id: 100008},
    {teacher_id: 100009, student_id: 100009},
    {teacher_id: 100010, student_id: 100010},
    {teacher_id: 100011, student_id: 100011}
        ])


SubjectGradeLevel.create ([
    {name: "Beginning Reading", description: "Beginners Reading"},
    {name: "Reading Skills 1", description: "1st Grade Reading Skills"},
    {name: "Reading Skills 2", description: "2nd Grade Reading Skills"},
    {name: "Reading Skills 3", description: "3rd Grade Reading Skills"},
    {name: "Reading Skills 4", description: "4th Grade Reading Skills"},
    {name: "Reading Skills 5", description: "5th Grade Reading Skills"},
    {name: "Reading Skills 6", description: "6th Grade Reading Skills"},
    {name: "Reading Skills 7", description: "7th Grade Reading Skills"},
    {name: "Reading Skills 8", description: "8th Grade Reading Skills"},
    {name: "Reading Skills 9", description: "9th Grade Reading Skills"},
    {name: "Reading Skills 10", description: "10th Grade Reading Skills"},
    {name: "Reading Skills 11", description: "11th Grade Reading Skills"},
    {name: "Reading Skills 12", description: "12th Grade Reading Skills"},
    {name: "Math Concepts 1", description: "1st Grade Math Concepts"},
    {name: "Math Concepts 2", description: "2nd Grade Math Concepts"},
    {name: "Math Concepts 3", description: "3rd Grade Math Concepts"},
    {name: "Math Concepts 4", description: "4th Grade Math Concepts"},
    {name: "Math Concepts 5", description: "5th Grade Math Concepts"},
    {name: "Math Concepts 6", description: "6th Grade Math Concepts"},
    {name: "Math Concepts 7", description: "7th Grade Math Concepts"},
    {name: "Math Concepts 8", description: "8th Grade Math Concepts"},
    {name: "Math Concepts 9", description: "9th Grade Math Concepts"},
    {name: "Math Concepts 10", description: "10th Grade Math Concepts"},
    {name: "Math Concepts 11", description: "11th Grade Math Concepts"},
    {name: "Math Concepts 12", description: "12th Grade Math Concepts"},
    {name: "Physics", description: "Physics-All Levels"},
    {name: "Biology", description: "Biology-All Levels"},
    {name: "Chemistry", description: "Chemistry-All Levels"},
    {name: "Gifted and Talented / AP classes", description: "Gifted and Talented / AP classes-All Levels"},
    {name: "Geometry", description: "Geometry-All Levels"},
    {name: "Algebra 1", description: "Algebra 1"},
    {name: "Algebra 2", description: "Algebra 2"},
    {name: "Algebra 3", description: "Algebra 3"},
    {name: "Exit level Math and Science (STAAR) ", description: "Exit level Math and Science (STAAR)"},
    {name: "Writing", description: "Writing-All Levels"},
    {name: "Grammar", description: "Grammar-All Levels"},
    {name: "SAT/ACT/THEA Preparation", description: "SAT/ACT/THEA Preparation"}
])



MedicalIssue.create([
    {issue_type: 'Allergy', description: 'Peanut Allergy', notes: 'This is the note field for medical issues.', student_id: 100001},
    {issue_type: 'Allergy', description: 'Peanut Allergy', notes: 'This is the note field for medical issues.', student_id: 100002},
    {issue_type: 'Allergy', description: 'Peanut Allergy', notes: 'This is the note field for medical issues.', student_id: 100003},
    {issue_type: 'Allergy', description: 'Peanut Allergy', notes: 'This is the note field for medical issues.', student_id: 100004},
    {issue_type: 'Medication', description: 'EpiPen', notes: 'This is the note field for medical issues.', student_id: 100001},
    {issue_type: 'Medication', description: 'EpiPen', notes: 'This is the note field for medical issues.', student_id: 100002},
    {issue_type: 'Medication', description: 'EpiPen', notes: 'This is the note field for medical issues.', student_id: 100003},
    {issue_type: 'Medication', description: 'EpiPen', notes: 'This is the note field for medical issues.', student_id: 100004}
    ])

