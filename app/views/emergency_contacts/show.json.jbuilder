json.extract! @emergency_contact, :id, :first_name, :middle_name, :last_name, :relationship, :primary_phone, :secondary_phone, :created_at, :updated_at
