json.array!(@emergency_contacts) do |emergency_contact|
  json.extract! emergency_contact, :id, :first_name, :middle_name, :last_name, :relationship, :primary_phone, :secondary_phone
  json.url emergency_contact_url(emergency_contact, format: :json)
end
