json.array!(@medical_issues) do |medical_issue|
  json.extract! medical_issue, :id, :issue_type, :description, :notes, :student_id
  json.url medical_issue_url(medical_issue, format: :json)
end
