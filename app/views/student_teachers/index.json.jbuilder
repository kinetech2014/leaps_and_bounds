json.array!(@student_teachers) do |student_teacher|
  json.extract! student_teacher, :id, :student_id, :teacher_id
  json.url student_teacher_url(student_teacher, format: :json)
end
