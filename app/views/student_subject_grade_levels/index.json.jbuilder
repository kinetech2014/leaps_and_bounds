json.array!(@student_subject_grade_levels) do |student_subject_grade_level|
  json.extract! student_subject_grade_level, :id
  json.url student_subject_grade_level_url(student_subject_grade_level, format: :json)
end
