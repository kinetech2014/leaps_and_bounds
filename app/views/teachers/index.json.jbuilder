json.array!(@teachers) do |teacher|
  json.extract! teacher, :id, :first_name, :middle_name, :last_name, :sex
  json.url teacher_url(teacher, format: :json)
end
