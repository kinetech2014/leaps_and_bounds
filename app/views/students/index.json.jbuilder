json.array!(@students) do |student|
  json.extract! student, :id, :first_name, :middle_name, :last_name, :birth_date, :sex, :tutoring_hours, :primary_phone, :secondary_phone, :email, :address1, :address2, :city, :state, :zip_code, :school_name
  json.url student_url(student, format: :json)
end
