json.array!(@student_guardians) do |student_guardian|
  json.extract! student_guardian, :id, :guardian_id, :student_id
  json.url student_guardian_url(student_guardian, format: :json)
end
