json.array!(@teacher_subject_grade_levels) do |teacher_subject_grade_level|
  json.extract! teacher_subject_grade_level, :id
  json.url teacher_subject_grade_level_url(teacher_subject_grade_level, format: :json)
end
