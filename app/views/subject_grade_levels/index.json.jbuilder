json.array!(@subject_grade_levels) do |subject_grade_level|
  json.extract! subject_grade_level, :id, :name, :description
  json.url subject_grade_level_url(subject_grade_level, format: :json)
end
