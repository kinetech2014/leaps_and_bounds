json.array!(@sign_in_outs) do |sign_in_out|
  json.extract! sign_in_out, :id, :date, :sign_in, :sign_out, :status, :student_id
  json.url sign_in_out_url(sign_in_out, format: :json)
end
