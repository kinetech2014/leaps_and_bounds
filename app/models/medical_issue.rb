class MedicalIssue < ActiveRecord::Base
  belongs_to :student

  #Checks if the value is either nil or a blank string
  validates :description, :presence => true

  #Validates associations with other models
  validates_associated :student

end
