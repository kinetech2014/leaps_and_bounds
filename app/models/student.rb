class Student < ActiveRecord::Base
  has_many :medical_issues, :dependent => :delete_all
  has_many :student_teachers, :dependent => :delete_all
  has_many :teachers, through: :student_teachers
  has_many :student_guardians, :dependent => :delete_all
  has_many :guardians, through: :student_guardians
  has_many :student_subject_grade_levels, :dependent => :delete_all
  has_many :subject_grade_levels, through: :student_subject_grade_levels
  has_many :sign_in_outs, :dependent => :delete_all
  has_many :emergency_contacts, :dependent => :delete_all



  mount_uploader :contract, ContractUploader


  def self.search(query)
    where("first_name like ? or last_name like ? or email like ?", "%#{query}%","%#{query}%","%#{query}%")
  end

  #This method combines the first, middle, & last name unless there's no middle name then it's just first & last name
  def full_name
    if self.middle_name == nil
      self.first_name + ' ' + self.last_name
    else
      self.first_name + ' ' + self.middle_name + ' ' + self.last_name
    end
  end

  #Checks if the value is either nil or a blank string
  validates :sex, :birth_date, :presence => true

  #Specifies length constraints
  validates :first_name, :last_name, :tutoring_hours, :address1, :city, :state, :zip_code, :length =>
      {
          :minimum   => 2,
          :maximum   => 128,
          :too_short => ": %{count} characters is the minimum allowed",
          :too_long  => ": %{count} characters is the minimum allowed"
      }

  validates :primary_phone, :length =>
      {
          :minimum   => 2,
          :maximum   => 20,
          :too_short => ": %{count} characters is the minimum allowed",
          :too_long  => ": %{count} characters is the minimum allowed"
      }
end
