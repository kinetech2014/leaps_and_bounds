class SignInOut < ActiveRecord::Base
  belongs_to :student

  #---Validations section---
  validates :student_id, :presence => {:message => 'Please input your Student ID number'}
  #---Methods section---

  #Returns on-site or off-site in sign-in/out status field
  def site_status
    if self.sign_out != nil
      return "Off-Site"
    else
      return "On-Site"
    end
  end

end

