class Guardian < ActiveRecord::Base
  def full_name
    if self.middle_name == nil
      self.first_name + ' ' + self.last_name
    else
      self.first_name + ' ' + self.middle_name + ' ' + self.last_name
    end
  end

  has_many :student_guardians, :dependent => :delete_all
  has_many :students, through: :student_guardians

  def self.search(query)
    where("first_name like ? or last_name like ? or email like ?", "%#{query}%","%#{query}%","%#{query}%")
  end

  #Checks if the value is either nil or a blank string
  validates :authorized_pickup, :sex, :presence => true

  #Specifies length constraints
  validates :first_name, :last_name, :relationship, :driver_license_number, :address1, :city, :state, :zip_code, :length =>
      {
          :minimum   => 2,
          :maximum   => 128,
          :too_short => ": %{count} characters is the minimum allowed",
          :too_long  => ": %{count} characters is the minimum allowed"
      }

  validates :primary_phone, :length =>
      {
          :minimum   => 2,
          :maximum   => 20,
          :too_short => ": %{count} characters is the minimum allowed",
          :too_long  => ": %{count} characters is the minimum allowed"
      }

end
