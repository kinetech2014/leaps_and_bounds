class Teacher < ActiveRecord::Base
  has_many :student_teachers, :dependent => :delete_all
  has_many :students, through: :student_teachers
  has_many :teacher_subject_grade_levels, :dependent => :delete_all
  has_many :subject_grade_levels, through: :teacher_subject_grade_levels


  def self.search(query)
    where("first_name like ? or last_name like ?", "%#{query}%","%#{query}%")
  end

  #---Validations area---

  #Checks if the value is either nil or a blank string
  validates :sex, :presence => true

  #Specifies length constraints
  validates :first_name, :last_name, :length =>
      {
          :minimum   => 2,
          :maximum   => 128,
          :too_short => ": %{count} characters is the minimum allowed",
          :too_long  => ": %{count} characters is the minimum allowed"
      }

  #---Methods area---

  #This method combines the first, middle, & last name unless there's no middle name then it's just first & last name
  def full_name
    if self.middle_name == nil
      self.first_name + ' ' + self.last_name
    else
      self.first_name + ' ' + self.middle_name + ' ' + self.last_name
    end
  end
end
