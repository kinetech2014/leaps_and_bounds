class EmergencyContact < ActiveRecord::Base
  def full_name
    if self.middle_name == nil
      self.first_name + ' ' + self.last_name
    else
      self.first_name + ' ' + self.middle_name + ' ' + self.last_name
    end
  end

  belongs_to :student

  def self.search(query)
    where("first_name like ? or last_name like ? or primary_phone like ?", "%#{query}%","%#{query}%","%#{query}%")
  end


  #Specifies length constraints
  validates :first_name, :last_name, :relationship, :length =>
      {
          :minimum   => 2,
          :maximum   => 128,
          :too_short => ": %{count} characters is the minimum allowed",
          :too_long  => ": %{count} characters is the minimum allowed"
      }

  validates :primary_phone, :length =>
      {
          :minimum   => 2,
          :maximum   => 20,
          :too_short => ": %{count} characters is the minimum allowed",
          :too_long  => ": %{count} characters is the minimum allowed"
      }

  #Validates associations with other models
  validates_associated :student

end
