class SubjectGradeLevel < ActiveRecord::Base
  has_many :student_subject_grade_levels, :dependent => :delete_all
  has_many :students, through: :student_subject_grade_levels
  has_many :teacher_subject_grade_levels, :dependent => :delete_all
  has_many :teachers, through: :teacher_subject_grade_levels

  #Specifies length constraints
  validates :name, :length =>
      {
          :minimum   => 2,
          :maximum   => 128,
          :too_short => ": %{count} characters is the minimum allowed",
          :too_long  => ": %{count} characters is the minimum allowed"
      }

  validates :description, :length =>
      {
          :minimum   => 2,
          :maximum   => 256,
          :too_short => ": %{count} characters is the minimum allowed",
          :too_long  => ": %{count} characters is the minimum allowed"
      }
end
