class StudentSubjectGradeLevel < ActiveRecord::Base
  belongs_to :student
  belongs_to :subject_grade_level
end
