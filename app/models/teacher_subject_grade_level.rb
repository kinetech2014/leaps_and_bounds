class TeacherSubjectGradeLevel < ActiveRecord::Base
  belongs_to :teacher
  belongs_to :subject_grade_level
end
