class StudentTeachersController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_student_teacher, only: [:show, :edit, :update, :destroy]

  # GET /student_teachers
  # GET /student_teachers.json
  def index
    @student_teachers = StudentTeacher.page(params[:page]).per(10)
  end

  # GET /student_teachers/1
  # GET /student_teachers/1.json
  def show
      if URI(request.referer).query.include? "student_id"
        redirect_to :action => 'show', :controller => 'students', :id => StudentTeacher.find(params[:id]).student_id
      elsif URI(request.referer).query.include? "teacher_id"
        redirect_to :action => 'show', :controller => 'teachers', :id => StudentTeacher.find(params[:id]).teacher_id
      end
    end

  # GET /student_teachers/new
  def new
    @student_teacher = StudentTeacher.new
  end

  # GET /student_teachers/1/edit
  def edit
  end

  # POST /student_teachers
  # POST /student_teachers.json
  def create
    @student_teacher = StudentTeacher.new(student_teacher_params)

    respond_to do |format|
      if @student_teacher.save
        format.html { redirect_to @student_teacher, notice: 'Student teacher was successfully created.' }
        format.json { render action: 'show', status: :created, location: @student_teacher }
      else
        format.html { render action: 'new' }
        format.json { render json: @student_teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /student_teachers/1
  # PATCH/PUT /student_teachers/1.json
  def update
    respond_to do |format|
      if @student_teacher.update(student_teacher_params)
        format.html { redirect_to @student_teacher, notice: 'Student teacher was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @student_teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /student_teachers/1
  # DELETE /student_teachers/1.json
  def destroy
    @student_teacher.destroy
    respond_to do |format|
      format.html { redirect_to request.referer }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student_teacher
      @student_teacher = StudentTeacher.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_teacher_params
      params.require(:student_teacher).permit(:student_id, :teacher_id)
    end
end
