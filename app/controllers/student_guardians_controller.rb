class StudentGuardiansController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_student_guardian, only: [:show, :edit, :update, :destroy]

  # GET /student_guardians
  # GET /student_guardians.json
  def index
    @student_guardians = StudentGuardian.all
  end

  # GET /student_guardians/1
  # GET /student_guardians/1.json
  def show
    if URI(request.referer).query.include? "student_id"
      redirect_to :action => 'show', :controller => 'students', :id => StudentGuardian.find(params[:id]).student_id
    elsif URI(request.referer).query.include? "guardian_id"
      redirect_to :action => 'show', :controller => 'guardians', :id => StudentGuardian.find(params[:id]).guardian_id
    end  end

  # GET /student_guardians/new
  def new
    @student_guardian = StudentGuardian.new
  end

  # GET /student_guardians/1/edit
  def edit
  end

  # POST /student_guardians
  # POST /student_guardians.json
  def create
    @student_guardian = StudentGuardian.new(student_guardian_params)

    respond_to do |format|
      if @student_guardian.save
        format.html { redirect_to @student_guardian, notice: 'Student guardian was successfully created.' }
        format.json { render action: 'show', status: :created, location: @student_guardian }
      else
        format.html { render action: 'new' }
        format.json { render json: @student_guardian.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /student_guardians/1
  # PATCH/PUT /student_guardians/1.json
  def update
    respond_to do |format|
      if @student_guardian.update(student_guardian_params)
        format.html { redirect_to @student_guardian, notice: 'Student guardian was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @student_guardian.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /student_guardians/1
  # DELETE /student_guardians/1.json
  def destroy
    @student_guardian.destroy
    respond_to do |format|
      format.html { redirect_to request.referer }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student_guardian
      @student_guardian = StudentGuardian.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_guardian_params
      params.require(:student_guardian).permit(:guardian_id, :student_id)
    end
end
