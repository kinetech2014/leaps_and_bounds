class SubjectGradeLevelsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_subject_grade_level, only: [:show, :edit, :update, :destroy]

  # GET /subject_grade_levels
  # GET /subject_grade_levels.json
  def index
    @subject_grade_levels = SubjectGradeLevel.page(params[:page]).per(10)
  end

  # GET /subject_grade_levels/1
  # GET /subject_grade_levels/1.json
  def show
  end

  # GET /subject_grade_levels/new
  def new
    @subject_grade_level = SubjectGradeLevel.new
  end

  # GET /subject_grade_levels/1/edit
  def edit
  end

  # POST /subject_grade_levels
  # POST /subject_grade_levels.json
  def create
    @subject_grade_level = SubjectGradeLevel.new(subject_grade_level_params)

    respond_to do |format|
      if @subject_grade_level.save
        format.html { redirect_to @subject_grade_level, notice: 'Subject grade level was successfully created.' }
        format.json { render action: 'show', status: :created, location: @subject_grade_level }
      else
        format.html { render action: 'new' }
        format.json { render json: @subject_grade_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subject_grade_levels/1
  # PATCH/PUT /subject_grade_levels/1.json
  def update
    respond_to do |format|
      if @subject_grade_level.update(subject_grade_level_params)
        format.html { redirect_to @subject_grade_level, notice: 'Subject grade level was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @subject_grade_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subject_grade_levels/1
  # DELETE /subject_grade_levels/1.json
  def destroy
    @subject_grade_level.destroy
    respond_to do |format|
      format.html { redirect_to request.referer }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subject_grade_level
      @subject_grade_level = SubjectGradeLevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subject_grade_level_params
      params.require(:subject_grade_level).permit(:name, :description)
    end
end
