class SignInOutsController < ApplicationController
  before_filter :authenticate_user!, only: :index
  before_action :set_sign_in_out, only: [:show, :edit, :update, :destroy]

  def student_exists(id_number)
    @student = Student.where(id: id_number)
    if @student.any?
      return true
    else
      return false
    end
  end

  # GET /sign_in_outs
  # GET /sign_in_outs.json
  def index
    @sign_in_outs = SignInOut.all
  end

  # GET /sign_in_outs/1
  # GET /sign_in_outs/1.json
  def show
  end

  # GET /sign_in_outs/new
  def new
    @sign_in_out = SignInOut.new
  end

  # GET /sign_in_outs/1/edit
  def edit
  end

  # POST /sign_in_outs
  # POST /sign_in_outs.json
  def create
    #@sign_in_out = SignInOut.new(sign_in_out_params)
    #temp = SignInOut.find_last_by_student_id(@sign_in_out.student_id)
    @sign_in_out = SignInOut.where(student_id: sign_in_out_params[:student_id], sign_out: nil).first_or_initialize
    p @sign_in_out.to_yaml
    if @sign_in_out.new_record?
      if params[:Sign_In]
        if student_exists(@sign_in_out.student_id)
          @sign_in_out.sign_in = Time.now
        else
          flash[:error] = "No student matches that Student ID!"
          error = 1
        end
      elsif params[:Sign_Out]
        flash[:error] = "You are not signed in!"
        error = 1
      end
    else
      if params[:Sign_In]
        flash[:error] = "You are already signed in!"
        error = 1
        #raise ActionController::RoutingError.new('Already Signed In')
      elsif params[:Sign_Out] && (@sign_in_out.sign_out == nil)
        @sign_in_out.sign_out = Time.now
      end
    end
    respond_to do |format|
      if !error && @sign_in_out.save
        #format.html { redirect_to @sign_in_out, notice: 'Sign in out was successfully created.' }
        if @sign_in_out.sign_out.nil?
          format.html { redirect_to new_sign_in_out_path, notice: 'Sign in was successful.' }
          format.json { render action: 'show', status: :created, location: @sign_in_out }
        else
          format.html { redirect_to new_sign_in_out_path, notice: 'Sign out was successful.' }
          format.json { render action: 'show', status: :created, location: @sign_in_out }
        end

      else
        format.html { render action: 'new' }
        format.json { render json: @sign_in_out.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sign_in_outs/1
  # PATCH/PUT /sign_in_outs/1.json
  def update
    redirect_to new_sign_in_out_path
    #@sign_in_out.sign_out = Time.now.strftime("%I:%M:%S %p")
    #respond_to do |format|
    #  if @sign_in_out.update(sign_in_out_params)
    #    format.html { redirect_to @sign_in_out, notice: 'Sign in out was successfully updated.' }
    #    format.json { head :no_content }
    #  else
    #    format.html { render action: 'edit' }
    #    format.json { render json: @sign_in_out.errors, status: :unprocessable_entity }
    #  end
    #end
    # Morgan and Alan's Platinum Seal of Approval V1.3 2014
  end

  # DELETE /sign_in_outs/1
  # DELETE /sign_in_outs/1.json
  def destroy
    @sign_in_out.destroy
    respond_to do |format|
      format.html { redirect_to sign_in_outs_url }
      format.json { head :no_content }
    end
  end

  def show_date_time
    render :text=>"Now : #{DateTime.now.to_s}"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sign_in_out
      @sign_in_out = SignInOut.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sign_in_out_params
      params.require(:sign_in_out).permit(:date, :status, :student_id)
    end
end
