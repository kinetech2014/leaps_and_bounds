class StudentSubjectGradeLevelsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_student_subject_grade_level, only: [:show, :edit, :update, :destroy]

  # GET /student_subject_grade_levels
  # GET /student_subject_grade_levels.json
  def index
    @student_subject_grade_levels = StudentSubjectGradeLevel.all
  end

  # GET /student_subject_grade_levels/1
  # GET /student_subject_grade_levels/1.json
  def show
    redirect_to :action => 'show', :controller => 'students', :id => StudentSubjectGradeLevel.find(params[:id]).student_id
  end

  # GET /student_subject_grade_levels/new
  def new
    @student_subject_grade_level = StudentSubjectGradeLevel.new
  end

  # GET /student_subject_grade_levels/1/edit
  def edit
  end

  # POST /student_subject_grade_levels
  # POST /student_subject_grade_levels.json
  def create
    @student_subject_grade_level = StudentSubjectGradeLevel.new(student_subject_grade_level_params)

    respond_to do |format|
      if @student_subject_grade_level.save
        format.html { redirect_to @student_subject_grade_level, notice: 'Student subject grade level was successfully created.' }
        format.json { render action: 'show', status: :created, location: @student_subject_grade_level }
      else
        format.html { render action: 'new' }
        format.json { render json: @student_subject_grade_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /student_subject_grade_levels/1
  # PATCH/PUT /student_subject_grade_levels/1.json
  def update
    respond_to do |format|
      if @student_subject_grade_level.update(student_subject_grade_level_params)
        format.html { redirect_to @student_subject_grade_level, notice: 'Student subject grade level was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @student_subject_grade_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /student_subject_grade_levels/1
  # DELETE /student_subject_grade_levels/1.json
  def destroy
    @student_subject_grade_level.destroy
    respond_to do |format|
      format.html { redirect_to request.referer }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student_subject_grade_level
      @student_subject_grade_level = StudentSubjectGradeLevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_subject_grade_level_params
      params.require(:student_subject_grade_level).permit(:student_id, :subject_grade_level_id)
    end
end
