class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def system_time
    respond_to do |format|
      format.js   {render text: DateTime.now.strftime('%I:%M:%S %p')}
      format.json {render json:{time:DateTime.now.strftime('%I:%M:%S %p')}, status: :ok}
      format.html {render text: DateTime.now.strftime('%I:%M:%S %p')}
    end
  end

end
