class MedicalIssuesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_medical_issue, only: [:show, :edit, :update, :destroy]

  # GET /medical_issues
  # GET /medical_issues.json
  def index
    @medical_issues = MedicalIssue.page(params[:page]).per(10)
  end

  # GET /medical_issues/1
  # GET /medical_issues/1.json
  def show
  end

  # GET /medical_issues/new
  def new
    @medical_issue = MedicalIssue.new
  end

  # GET /medical_issues/1/edit
  def edit
  end

  # POST /medical_issues
  # POST /medical_issues.json
  def create
    @medical_issue = MedicalIssue.new(medical_issue_params)

    respond_to do |format|
      if @medical_issue.save
        format.html { redirect_to @medical_issue, notice: 'Medical issue was successfully created.' }
        format.json { render action: 'show', status: :created, location: @medical_issue }
      else
        format.html { render action: 'new' }
        format.json { render json: @medical_issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /medical_issues/1
  # PATCH/PUT /medical_issues/1.json
  def update
    respond_to do |format|
      if @medical_issue.update(medical_issue_params)
        format.html { redirect_to @medical_issue, notice: 'Medical issue was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @medical_issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /medical_issues/1
  # DELETE /medical_issues/1.json
  def destroy
    @medical_issue.destroy
    respond_to do |format|
      format.html { redirect_to request.referer }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_medical_issue
      @medical_issue = MedicalIssue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def medical_issue_params
      params.require(:medical_issue).permit(:issue_type, :description, :notes, :student_id)
    end
end
