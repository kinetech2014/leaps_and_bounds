class TeacherSubjectGradeLevelsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_teacher_subject_grade_level, only: [:show, :edit, :update, :destroy]

  # GET /teacher_subject_grade_levels
  # GET /teacher_subject_grade_levels.json
  def index
    @teacher_subject_grade_levels = TeacherSubjectGradeLevel.all
  end

  # GET /teacher_subject_grade_levels/1
  # GET /teacher_subject_grade_levels/1.json
  def show
    redirect_to :action => 'show', :controller => 'teachers', :id => TeacherSubjectGradeLevel.find(params[:id]).teacher_id
  end

  # GET /teacher_subject_grade_levels/new
  def new
    @teacher_subject_grade_level = TeacherSubjectGradeLevel.new
  end

  # GET /teacher_subject_grade_levels/1/edit
  def edit
  end

  # POST /teacher_subject_grade_levels
  # POST /teacher_subject_grade_levels.json
  def create
    @teacher_subject_grade_level = TeacherSubjectGradeLevel.new(teacher_subject_grade_level_params)

    respond_to do |format|
      if @teacher_subject_grade_level.save
        format.html { redirect_to @teacher_subject_grade_level, notice: 'Teacher subject grade level was successfully created.' }
        format.json { render action: 'show', status: :created, location: @teacher_subject_grade_level }
      else
        format.html { render action: 'new' }
        format.json { render json: @teacher_subject_grade_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /teacher_subject_grade_levels/1
  # PATCH/PUT /teacher_subject_grade_levels/1.json
  def update
    respond_to do |format|
      if @teacher_subject_grade_level.update(teacher_subject_grade_level_params)
        format.html { redirect_to @teacher_subject_grade_level, notice: 'Teacher subject grade level was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @teacher_subject_grade_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /teacher_subject_grade_levels/1
  # DELETE /teacher_subject_grade_levels/1.json
  def destroy
    @teacher_subject_grade_level.destroy
    respond_to do |format|
      format.html { redirect_to request.referer }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_teacher_subject_grade_level
      @teacher_subject_grade_level = TeacherSubjectGradeLevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def teacher_subject_grade_level_params
      params.require(:teacher_subject_grade_level).permit(:teacher_id, :subject_grade_level_id)
    end
end
