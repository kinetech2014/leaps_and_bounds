# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

getSystemTime = ->
  $.ajax({
    url:'/system_time'
    dataType:'script'
    complete: (data)->
      $('#system_time').html(data.responseText)
  })

$ ->
  getSystemTime()
  setInterval(->
    getSystemTime()
  , 1000
  )