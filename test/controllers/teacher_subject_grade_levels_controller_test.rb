require 'test_helper'

class TeacherSubjectGradeLevelsControllerTest < ActionController::TestCase
  setup do
    @teacher_subject_grade_level = teacher_subject_grade_levels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:teacher_subject_grade_levels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create teacher_subject_grade_level" do
    assert_difference('TeacherSubjectGradeLevel.count') do
      post :create, teacher_subject_grade_level: {  }
    end

    assert_redirected_to teacher_subject_grade_level_path(assigns(:teacher_subject_grade_level))
  end

  test "should show teacher_subject_grade_level" do
    get :show, id: @teacher_subject_grade_level
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @teacher_subject_grade_level
    assert_response :success
  end

  test "should update teacher_subject_grade_level" do
    patch :update, id: @teacher_subject_grade_level, teacher_subject_grade_level: {  }
    assert_redirected_to teacher_subject_grade_level_path(assigns(:teacher_subject_grade_level))
  end

  test "should destroy teacher_subject_grade_level" do
    assert_difference('TeacherSubjectGradeLevel.count', -1) do
      delete :destroy, id: @teacher_subject_grade_level
    end

    assert_redirected_to teacher_subject_grade_levels_path
  end
end
