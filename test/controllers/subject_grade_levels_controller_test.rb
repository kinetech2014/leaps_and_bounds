require 'test_helper'

class SubjectGradeLevelsControllerTest < ActionController::TestCase
  setup do
    @subject_grade_level = subject_grade_levels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:subject_grade_levels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create subject_grade_level" do
    assert_difference('SubjectGradeLevel.count') do
      post :create, subject_grade_level: { description: @subject_grade_level.description, name: @subject_grade_level.name }
    end

    assert_redirected_to subject_grade_level_path(assigns(:subject_grade_level))
  end

  test "should show subject_grade_level" do
    get :show, id: @subject_grade_level
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @subject_grade_level
    assert_response :success
  end

  test "should update subject_grade_level" do
    patch :update, id: @subject_grade_level, subject_grade_level: { description: @subject_grade_level.description, name: @subject_grade_level.name }
    assert_redirected_to subject_grade_level_path(assigns(:subject_grade_level))
  end

  test "should destroy subject_grade_level" do
    assert_difference('SubjectGradeLevel.count', -1) do
      delete :destroy, id: @subject_grade_level
    end

    assert_redirected_to subject_grade_levels_path
  end
end
