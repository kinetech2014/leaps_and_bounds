require 'test_helper'

class StudentTeachersControllerTest < ActionController::TestCase
  setup do
    @student_teacher = student_teachers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:student_teachers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create student_teacher" do
    assert_difference('StudentTeacher.count') do
      post :create, student_teacher: { student_id: @student_teacher.student_id, teacher_id: @student_teacher.teacher_id }
    end

    assert_redirected_to student_teacher_path(assigns(:student_teacher))
  end

  test "should show student_teacher" do
    get :show, id: @student_teacher
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @student_teacher
    assert_response :success
  end

  test "should update student_teacher" do
    patch :update, id: @student_teacher, student_teacher: { student_id: @student_teacher.student_id, teacher_id: @student_teacher.teacher_id }
    assert_redirected_to student_teacher_path(assigns(:student_teacher))
  end

  test "should destroy student_teacher" do
    assert_difference('StudentTeacher.count', -1) do
      delete :destroy, id: @student_teacher
    end

    assert_redirected_to student_teachers_path
  end
end
