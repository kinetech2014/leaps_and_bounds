require 'test_helper'

class GuardiansControllerTest < ActionController::TestCase
  setup do
    @guardian = guardians(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:guardians)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create guardian" do
    assert_difference('Guardian.count') do
      post :create, guardian: { address1: @guardian.address1, address2: @guardian.address2, authorized_pickup: @guardian.authorized_pickup, city: @guardian.city, driver_license_number: @guardian.driver_license_number, email: @guardian.email, first_name: @guardian.first_name, last_name: @guardian.last_name, middle_name: @guardian.middle_name, primary_phone: @guardian.primary_phone, relationship: @guardian.relationship, secondary_phone: @guardian.secondary_phone, sex: @guardian.sex, state: @guardian.state, zip_code: @guardian.zip_code }
    end

    assert_redirected_to guardian_path(assigns(:guardian))
  end

  test "should show guardian" do
    get :show, id: @guardian
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @guardian
    assert_response :success
  end

  test "should update guardian" do
    patch :update, id: @guardian, guardian: { address1: @guardian.address1, address2: @guardian.address2, authorized_pickup: @guardian.authorized_pickup, city: @guardian.city, driver_license_number: @guardian.driver_license_number, email: @guardian.email, first_name: @guardian.first_name, last_name: @guardian.last_name, middle_name: @guardian.middle_name, primary_phone: @guardian.primary_phone, relationship: @guardian.relationship, secondary_phone: @guardian.secondary_phone, sex: @guardian.sex, state: @guardian.state, zip_code: @guardian.zip_code }
    assert_redirected_to guardian_path(assigns(:guardian))
  end

  test "should destroy guardian" do
    assert_difference('Guardian.count', -1) do
      delete :destroy, id: @guardian
    end

    assert_redirected_to guardians_path
  end
end
