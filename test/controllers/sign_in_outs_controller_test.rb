require 'test_helper'

class SignInOutsControllerTest < ActionController::TestCase
  setup do
    @sign_in_out = sign_in_outs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sign_in_outs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sign_in_out" do
    assert_difference('SignInOut.count') do
      post :create, sign_in_out: { date: @sign_in_out.date, sign_in: @sign_in_out.sign_in, sign_out: @sign_in_out.sign_out, status: @sign_in_out.status, student_id: @sign_in_out.student_id }
    end

    assert_redirected_to sign_in_out_path(assigns(:sign_in_out))
  end

  test "should show sign_in_out" do
    get :show, id: @sign_in_out
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sign_in_out
    assert_response :success
  end

  test "should update sign_in_out" do
    patch :update, id: @sign_in_out, sign_in_out: { date: @sign_in_out.date, sign_in: @sign_in_out.sign_in, sign_out: @sign_in_out.sign_out, status: @sign_in_out.status, student_id: @sign_in_out.student_id }
    assert_redirected_to sign_in_out_path(assigns(:sign_in_out))
  end

  test "should destroy sign_in_out" do
    assert_difference('SignInOut.count', -1) do
      delete :destroy, id: @sign_in_out
    end

    assert_redirected_to sign_in_outs_path
  end
end
