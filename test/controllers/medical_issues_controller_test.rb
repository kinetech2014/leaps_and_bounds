require 'test_helper'

class MedicalIssuesControllerTest < ActionController::TestCase
  setup do
    @medical_issue = medical_issues(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:medical_issues)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create medical_issue" do
    assert_difference('MedicalIssue.count') do
      post :create, medical_issue: { description: @medical_issue.description, name: @medical_issue.name, notes: @medical_issue.notes, student_id: @medical_issue.student_id, type: @medical_issue.type }
    end

    assert_redirected_to medical_issue_path(assigns(:medical_issue))
  end

  test "should show medical_issue" do
    get :show, id: @medical_issue
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @medical_issue
    assert_response :success
  end

  test "should update medical_issue" do
    patch :update, id: @medical_issue, medical_issue: { description: @medical_issue.description, name: @medical_issue.name, notes: @medical_issue.notes, student_id: @medical_issue.student_id, type: @medical_issue.type }
    assert_redirected_to medical_issue_path(assigns(:medical_issue))
  end

  test "should destroy medical_issue" do
    assert_difference('MedicalIssue.count', -1) do
      delete :destroy, id: @medical_issue
    end

    assert_redirected_to medical_issues_path
  end
end
