require 'test_helper'

class StudentSubjectGradeLevelsControllerTest < ActionController::TestCase
  setup do
    @student_subject_grade_level = student_subject_grade_levels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:student_subject_grade_levels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create student_subject_grade_level" do
    assert_difference('StudentSubjectGradeLevel.count') do
      post :create, student_subject_grade_level: {  }
    end

    assert_redirected_to student_subject_grade_level_path(assigns(:student_subject_grade_level))
  end

  test "should show student_subject_grade_level" do
    get :show, id: @student_subject_grade_level
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @student_subject_grade_level
    assert_response :success
  end

  test "should update student_subject_grade_level" do
    patch :update, id: @student_subject_grade_level, student_subject_grade_level: {  }
    assert_redirected_to student_subject_grade_level_path(assigns(:student_subject_grade_level))
  end

  test "should destroy student_subject_grade_level" do
    assert_difference('StudentSubjectGradeLevel.count', -1) do
      delete :destroy, id: @student_subject_grade_level
    end

    assert_redirected_to student_subject_grade_levels_path
  end
end
