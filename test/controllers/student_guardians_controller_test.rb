require 'test_helper'

class StudentGuardiansControllerTest < ActionController::TestCase
  setup do
    @student_guardian = student_guardians(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:student_guardians)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create student_guardian" do
    assert_difference('StudentGuardian.count') do
      post :create, student_guardian: { guardian_id: @student_guardian.guardian_id, student_id: @student_guardian.student_id }
    end

    assert_redirected_to student_guardian_path(assigns(:student_guardian))
  end

  test "should show student_guardian" do
    get :show, id: @student_guardian
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @student_guardian
    assert_response :success
  end

  test "should update student_guardian" do
    patch :update, id: @student_guardian, student_guardian: { guardian_id: @student_guardian.guardian_id, student_id: @student_guardian.student_id }
    assert_redirected_to student_guardian_path(assigns(:student_guardian))
  end

  test "should destroy student_guardian" do
    assert_difference('StudentGuardian.count', -1) do
      delete :destroy, id: @student_guardian
    end

    assert_redirected_to student_guardians_path
  end
end
