# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
LeapsAndBounds::Application.config.secret_key_base = '8cb23a6229dc4af14a9c05f8d59f9b4f581977d4e5393579bbfdffbfd9e741117dcfb3f555e13e9be24802794d733971b62c93fd979cb436c5877dcbb2baf183'
